// Setup your quiz text and questions here

// NOTE: pay attention to commas, IE struggles with those bad boys

var quizJSON = {
    "info": {
        "name":    "Philosophy is the art of inquiry.",
        "main":    "",
        "results": "(To be Continued..)",
        "level1":  "Ready",
        "level2":  "Contender",
        "level3":  "Amateur",
        "level4":  "Newb",
        "level5":  "Stay in school, kid..." // no comma here
    },
    "questions": [
        { // Question 1
            "q": "Rene Descartes believed that one had to first start with the proof of one's own existence before one could answer any other question with certainty. What was his proof?",
            "a": [
                {"option": "Let us first believe.",      "correct": false},
                {"option": "My soul yet lives.",         "correct": false},
                {"option": "I think, therefore I am.",   "correct": true},
                {"option": "I myself am here.",          "correct": false} // no comma here
            ],
            "correct": "<p><span>That's right!</span> The letter A is the first letter in the alphabet!</p>",
            "incorrect": "<p><span>Uhh no.</span></p>" // no comma here
        },
        { // Question 2
            "q": "Physics was once part of philosophy and was known as 'natural philosophy'.",
            "a": [
                {"option": "False",    "correct": false},
                {"option": "True",     "correct": true}
            ],
            "correct": "<p><span>Holy bananas!</span> ..Yeh that was predictable.</p>",
            "incorrect": "<p><span>Fail.</span> Sorry. You lose.</p>" // no comma here
        },
        { // Question 3
            "q": "Einstein's theory of relativity says that space and time are not fixed, but are relative to the observer. The idea of space and time not being fixed entities was also one of the major theories of this German thinker, famous for his 'Critique of Pure Reason'. Who was he?",
            "a": [
                {"option": "Immanuel Kant",             "correct": true},
                {"option": "Antonidas Chocolate Cake",  "correct": false},
                {"option": "Grocery Store",             "correct": false},
                {"option": "Einstein",                  "correct": false} // no comma here
            ],
            "correct": "<p><span>Nice!</span> Kant drew a clear distinction between the reality of things, and what we perceive to be reality. He stated that we do not directly experience events, but instead experience our interpretation of these events. Part of this theory claimed that space and time were not part of the external world, but were concepts created by the mind so as to allow it to compute the information it takes in. The importance here is the challenge to concepts such as space and time as absolute and unchanging. Indeed, Einstein later used mathematical evidence to state that space and time are relative to the observer, as part of his General Theory of Relativity. For example, a person moving close to the speed of light will appear, relative to a stationary observer, to be extremely short. The major difference between Kant and Einstein is that Einstein considered space and time to be entities of the physical universe, whereas Kant viewed them as necessary creations of our minds. </p>",
            "incorrect": "<p><span>No.</span> Kant drew a clear distinction between the reality of things, and what we perceive to be reality. He stated that we do not directly experience events, but instead experience our interpretation of these events. Part of this theory claimed that space and time were not part of the external world, but were concepts created by the mind so as to allow it to compute the information it takes in. The importance here is the challenge to concepts such as space and time as absolute and unchanging. Indeed, Einstein later used mathematical evidence to state that space and time are relative to the observer, as part of his General Theory of Relativity. For example, a person moving close to the speed of light will appear, relative to a stationary observer, to be extremely short. The major difference between Kant and Einstein is that Einstein considered space and time to be entities of the physical universe, whereas Kant viewed them as necessary creations of our minds. </p>" // no comma here
        } // no comma here
    ]
};
