$(document).ready(function() {
 	//TweenLite.to("#char", 2, {width:100});
 	//TweenLite.from("#char", 2, {scaleX:1.5, scaleY:1.5, delay:1});
 	
 	//TweenLite.to("#slickQuiz", 5, {boxShadow:"0px 0px 20px black", color:"#064e54"})
 	
 	//TweenLite.to(photo, 1.5, {width:100, delay:0.5, onComplete:myFunction});

 	//TweenLite.to("#slickQuiz", 10, {backgroundColor:"#b1e9ee", ease:Power2.easeOut});

 	//TweenMax.from("#char", 2, {clip:"rect(800px 655px 0 0)"});


  $('#btn-allowCom').click(function (){
      $('#comPanelUI').slideUp();
      $('.stdTextCom').remove();
      $('.comInterface').remove();
      setTimeout(function (){
        teleport('#kitsuneWrap', '#1a98a1').done(kitsuneIntro);
      }, 1000);
  });

});

$(function () {
    $('#slickQuiz').slickQuiz();
});

function teleport(charWrap, color, speed = 12) {
  //initialize variables
  var defer = $.Deferred();
  var charImage = $(charWrap).find('img');
  var canvasId = $(charWrap).find('canvas').attr('id');
  var canvas = document.getElementById(canvasId);
  var cx = canvas.getContext('2d');
  var length = 0;
  var lFade = new Date();
  var lCircle = new Date();
  var posx = canvas.width / 2;
  var posy1 = canvas.height / 2;
  var posy2 = canvas.height / 2;
  var x2 = 0;
  var x3 = 0;
  
  //options
  var lineWidth = 1;
  var strokeStyle = color;
  
  //go
  tpPhaseOne();

  return defer;
  
  function tpPhaseOne() {
    
    var now = new Date();
    
    if (now - lFade > 20){
      cx.fillStyle = "rgba(238,238,238,0.2)"
      cx.fillRect(0, 0, canvas.width, canvas.height);
      lFade = now;
    }
    
    if (posy1 > 5){
      drawCircle(posx, posy1);
      drawCircle(posx, posy2);
      posy1 = posy1 - 5;
      posy2 = posy2 + 5;
    }
    else if ( x2 < canvas.width / 2 - 5){
      drawCircle(posx + x2, posy1);
      drawCircle(posx - x2, posy1);
      drawCircle(posx + x2, posy2);
      drawCircle(posx - x2, posy2);
      x2 = x2 + 5;
    } 
      else {
        endPhaseOne(0);
        return;
      }
    
    setTimeout(tpPhaseOne, 1000/100);
  }
  
  function endPhaseOne(x){
    drawCircle(5 + x, 5);
    drawCircle(canvas.width - 5 - x, 5);
    drawCircle(canvas.width - 5 - x, canvas.height - 5);
    drawCircle(5 + x, canvas.height - 5);
    var x = x + 5;
    
    if (x < canvas.width / 2) {
      setTimeout(function (){
        endPhaseOne(x);
      }, 1000/80);
    }
    else {
      tpPhaseTwo();
    }
  }
  
  function tpPhaseTwo() {
    length = length + speed;
    
    drawLines(length);
    if(length < (canvas.height - 10)){
      setTimeout(tpPhaseTwo, 15);
    } 
    else {
      TweenMax.to(charImage, 2, {alpha: 1});
      TweenMax.to('#' + canvasId, 2, {alpha: 0});
      setTimeout(function () {
        defer.resolve();
        $('#' + canvasId).remove(); 
      }, 2000);
    }

  }
  
  function drawLines(length){
    var x = 5;
    while (x < canvas.width) {
      var newpos = [x, 5];
      drawDown(newpos, length);
      var newposup = [x + 5, canvas.height - 5];
      if ( x < canvas.width - 5)
        drawUp(newposup,length);
      var x = x + 10;
    }
  }
  
  function drawDown(pos, len) {
    cx.beginPath();
    cx.moveTo(pos[0], pos[1]);
    cx.lineTo(pos[0], pos[1]+len);     
    cx.lineWidth = lineWidth;
    cx.strokeStyle = strokeStyle;
    cx.stroke();
    
    return [pos[0], pos[1]+len];
  }
  
  function drawUp(pos, len) {
    cx.beginPath();
    cx.moveTo(pos[0], pos[1]);
    cx.lineTo(pos[0], pos[1]-len);     
    cx.lineWidth = lineWidth;
    cx.strokeStyle = strokeStyle;
    cx.stroke();
    
    return [pos[0], pos[1]-len];
  }

  function drawCircle(x,y) {
    var radius = 2;
    cx.beginPath();
    cx.arc(x, y, radius, 0, 2 * Math.PI, false);
    cx.fillStyle = strokeStyle;
    cx.fill();
    cx.lineWidth = 1;
    cx.strokeStyle = strokeStyle;
    cx.stroke();
  }  
} //end of teleport

function kitsuneIntro() {
  var introHtml1 = '<div class="stdTextCom"><p>Hello.. My name is Kitsune.</p></div>';
  var introHtml2 = '<p>I was born in 2085 and I am here asking for your help.</p>';
  var introHtml3 = '<p>Planet Earth is no longer suitable for life.</p>';
  var introHtml4 = '<p>We spent our last resources building a device which enabled backward TCP/IP communication.</p>';
  var introHtml5 = '<p>Yes, my friend here is able to slash through space-time.</p>';
  var introHtml6 = '<p>Our aim is to send information that would allow someone like you to prevent the life sustainability index from reaching critical levels.</p>';
  var introHtml7 = '<p>The question is, are you ready for it?</p>';
  
  $('#comPanelUI').slideDown(function(){
    $(introHtml1).prependTo('#comPanelUI').fadeIn('slow', function(){
      setTimeout(function() {
        $(introHtml2).appendTo('.stdTextCom').fadeIn('slow')
      }, 1000);
      setTimeout(function() {
        $(introHtml3).appendTo('.stdTextCom').fadeIn('slow')
      }, 2500);
      setTimeout(function() {
        $(introHtml4).appendTo('.stdTextCom').fadeIn('slow')
      }, 4000);
      setTimeout(function() {
        $(introHtml5).appendTo('.stdTextCom').fadeIn('slow')
      }, 6000);
      setTimeout(function() {
        $(introHtml6).appendTo('.stdTextCom').fadeIn('slow')
      }, 8000);
      setTimeout(function() {
        $(introHtml7).appendTo('.stdTextCom').fadeIn('slow')
      }, 12000);
      setTimeout(askFirstMission, 16000);
    });
  }); 
}

function askFirstMission() {
  $('.stdTextCom').fadeOut('slow', function(){
    $('.stdTextCom').remove();
    var html = '<div class="stdTextCom"><p><strong>First Mission: Recycle!</strong></p></div><div class="comInterface"><span class="button" id="btn-Mission1">Iniciate Mission</span><span class="button" id="btn-denyCom">Abort Mission</span></div>'
    $(html).prependTo('#comPanelUI').fadeIn('slow');

    $('#btn-Mission1').click(function (){
      $('#comPanelUI').slideUp();
      $('.stdTextCom').remove();
      $('.comInterface').remove();
      setTimeout(function (){
          sortWaste();
      }, 1000);
    });
  });
}

function sortWaste() {
  var introtext = '<div class="stdTextCom"><p><strong>Instructions: </strong>Drag each waste item to the appropriate bin. <span style="float: right">Score: <span id="scoreValue">0</span> / 6</span></p></div><div class="comInterface"><span class="button" id="btn-startWaste">Start!</span></div>';
  $('#comPanelUI').prepend(introtext);
  $('#comPanelUI').slideDown();
  $('#btn-startWaste').click(startSortGame);

  var score = 0;
  var repeat = 0;

  function startSortGame () {
    $('.comInterface').hide();
    TweenMax.to('#comPanelUI', 1, {height: 500});
    $('#sortWasteWrapper').fadeIn('slow');
    score = 0;
    $('#scoreValue').html(score);
    var wrongPng = "images/waste/wrong.png";
    var rightPng = "images/waste/right.png";


    $(".wasteItem").draggable({ containment: "#sortWasteWrapper", helper: "clone", appendTo: "#sortWasteWrapper"});

    $('#binBlue').droppable({ hoverClass: 'border', tolerance: 'pointer', accept: '.wasteItem', 
                          drop: function(e, ui) {
                            ui.draggable.fadeOut();
                            if (ui.draggable.hasClass('paper')) {
                              score++;
                              $('#scoreValue').html(score);
                              $(this).append($("<img class='rightWrong'>").attr("src", rightPng));
                            } else {
                              $(this).append($("<img class='rightWrong'>").attr("src", wrongPng));
                            }
                            setTimeout(function() {
                              $('.rightWrong').remove();
                            }, 1000);
                          }
                          });

    $('#binGreen').droppable({ hoverClass: 'border', tolerance: 'pointer', accept: '.wasteItem', 
                          drop: function(e, ui) {
                            ui.draggable.fadeOut();
                            if (ui.draggable.hasClass('glass')) {
                              score++;
                              $('#scoreValue').html(score);
                              $(this).append($("<img class='rightWrong'>").attr("src", rightPng));
                            } else {
                              $(this).append($("<img class='rightWrong'>").attr("src", wrongPng));
                            }
                            setTimeout(function() {
                              $('.rightWrong').remove();
                            }, 1000);
                          }
                          });

    $('#binYellow').droppable({ hoverClass: 'border', tolerance: 'pointer', accept: '.wasteItem', 
                          drop: function(e, ui) {
                            ui.draggable.fadeOut();
                            if (ui.draggable.hasClass('metal')) {
                              score++;
                              $('#scoreValue').html(score);
                              $(this).append($("<img class='rightWrong'>").attr("src", rightPng));
                            } else {
                              $(this).append($("<img class='rightWrong'>").attr("src", wrongPng));
                            }
                            setTimeout(function() {
                              $('.rightWrong').remove();
                            }, 1000);
                          }
                          });

    $('#binBrown').droppable({ hoverClass: 'border', tolerance: 'pointer', accept: '.wasteItem', 
                          drop: function(e, ui) {
                            ui.draggable.fadeOut();
                            if (ui.draggable.hasClass('bio')) {
                              score++;
                              $('#scoreValue').html(score);
                              $(this).append($("<img class='rightWrong'>").attr("src", rightPng));
                            } else {
                              $(this).append($("<img class='rightWrong'>").attr("src", wrongPng));
                            }
                            setTimeout(function() {
                              $('.rightWrong').remove();
                            }, 1000);
                          }
                          });

    $('#binBlack').droppable({ hoverClass: 'border', tolerance: 'pointer', accept: '.wasteItem', 
                          drop: function(e, ui) {
                            ui.draggable.fadeOut();
                            if (ui.draggable.hasClass('general')) {
                              score++;
                              $('#scoreValue').html(score);
                              $(this).append($("<img class='rightWrong'>").attr("src", rightPng));
                            } else {
                              $(this).append($("<img class='rightWrong'>").attr("src", wrongPng));
                            }
                            setTimeout(function() {
                              $('.rightWrong').remove();
                            }, 1000);
                          }
                          });

    setTimeout(function () {
      $('#wasteItem1').fadeIn('slow');
      TweenMax.to('#wasteItem1', 6, {left: 640, ease:Linear.easeNone, onComplete: function() {
        $('#wasteItem1').fadeOut('slow');
      }});
    }, 1000);

    setTimeout(function () {
      $('#wasteItem2').fadeIn('slow');
      TweenMax.to('#wasteItem2', 6, {left: 640, ease:Linear.easeNone, onComplete: function() {
        $('#wasteItem2').fadeOut('slow');
      }});
    }, 3000);

    setTimeout(function () {
      $('#wasteItem3').fadeIn('slow');
      TweenMax.to('#wasteItem3', 6, {left: 640, ease:Linear.easeNone, onComplete: function() {
        $('#wasteItem3').fadeOut('slow');
      }});
    }, 5000);

    setTimeout(function () {
      $('#wasteItem4').fadeIn('slow');
      TweenMax.to('#wasteItem4', 6, {left: 640, ease:Linear.easeNone, onComplete: function() {
        $('#wasteItem4').fadeOut('slow');
      }});
    }, 7000);

    setTimeout(function () {
      $('#wasteItem5').fadeIn('slow');
      TweenMax.to('#wasteItem5', 6, {left: 640, ease:Linear.easeNone, onComplete: function() {
        $('#wasteItem5').fadeOut('slow');
      }});
    }, 9000);

    setTimeout(function () {
      $('#wasteItem6').fadeIn('slow');
      TweenMax.to('#wasteItem6', 6, {left: 640, ease:Linear.easeNone, onComplete: function() {
        $('#wasteItem6').fadeOut('slow', endSortGame);
      }});
    }, 11000);

  }

  function endSortGame() {
    $('.wasteItem').css({'position': 'absolute', 'left': '0', 'margin': '0 10px 10px 0', 'display': 'none'})
    var winHtml = '<p id="gameMessage">Well done, you have succeeded this mission!</p>';
    var loseHtml = '<p id="gameMessage">Too bad, try again!</p>';

    $('#sortWasteWrapper').fadeOut('fast', function() {
      if (score == 6) {
        $(winHtml).appendTo('.stdTextCom').fadeIn('fast');
        setTimeout(function (){
          $("#gameMessage").remove();
          $('.stdTextCom').fadeOut('normal', function(){
            $(this).remove();
            $('.comInterface').remove();
            $('#sortWasteWrapper').remove();
            Mission2();
          });
        }, 3000);
      } 
      else {
        $(loseHtml).appendTo('.stdTextCom').fadeIn('fast');
        repeat = 1;
        setTimeout(function (){
          $("#gameMessage").remove();
          $('.comInterface').fadeIn();
        }, 2000);
      }
    });  
  }
}

function Mission2() {
  var html = '<div class="stdTextCom"><p>It is time for you to meet Jin.</p><p>He will guide you on the next mission.</p><p>Good luck and farewell.</p></div>';
  $(html).appendTo('#comPanelUI').fadeIn();

  setTimeout(function (){
    setTimeout(function (){
      teleport('#monkWrap', '#1a98a1').done(monkIntro);
    }, 2000);
    $('#comPanelUI').slideUp();
    $('.stdTextCom').remove();
    $('#kitsune').fadeOut();
  }, 3000);
}

function monkIntro() {
  floatMonk();
  
  var html = '<div class="stdTextCom" style="margin-bottom: 150px;"><p>There is much to discuss, much to learn!</p><p>But first I must see where do you stand..</div><div class="comInterface"><span class="button" id="btn-Mission2">Iniciate Mission</span><span class="button" id="btn-denyCom">Abort Mission</span></div>';
  $('#comPanelUI').css({'height': 'auto'});
  $('#comPanelUI').slideDown();
  $(html).prependTo('#comPanelUI').fadeIn('slow');
  
  $('#btn-Mission2').click(function (){
    $('#comPanelUI').slideUp();
    $('.stdTextCom').remove();
    $('.comInterface').remove();
    setTimeout(function (){
      $('#comPanelUI').slideDown();
      $('#slickQuiz').fadeIn(1000);
    }, 1000);
  });
}

function floatMonk() {
  TweenMax.to('#monk', 2, {y: -30, x: -10, repeat: -1, yoyo: true, ease:Quad.easeInOut});
}
